package semenov.task2;

import java.util.Arrays;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Vector {
    final static int COORDINATE_COUNT = 3;
    final static String ERROR_CREATION_MESSAGE_LAYOUT = "Input should be integer array containing %d coordinates";
    final static String ERROR_CREATION_MESSAGE = String.format(ERROR_CREATION_MESSAGE_LAYOUT, COORDINATE_COUNT);

    final private int[] COORDINATE_ARR;
    final private double LENGTH;


    public Vector(int... coordinates) {
        COORDINATE_ARR = coordinates;
        checkInputArray();
        LENGTH = computeLength();
    }


    public double getLength() {
        return LENGTH;
    }

    public int getScalarMultiplyResult(Vector vector) {
        int result = 0;
        for (int i = 0; i < COORDINATE_COUNT; i++) {
            for (int j = 0; j < COORDINATE_COUNT; j++) {
                if (i == j) {
                    result += COORDINATE_ARR[i] * vector.COORDINATE_ARR[j];
                    break;
                }
            }
        }
        return result;
    }

    public Vector mupltiply(Vector vector) {
        int[] coordinateArr = new int[COORDINATE_COUNT];
        for (int i = 0; i < COORDINATE_COUNT; i++) {
            int newCoordinate = computeNewCoordinateForMultiply(vector, i);
            int newPosition = computePositionForCoordinateArr(i);
            coordinateArr[newPosition] = newCoordinate;
        }
        return new Vector(coordinateArr);
    }


    private double computeLength() {
        double squirtSum = Arrays.stream(COORDINATE_ARR)
                .mapToDouble(i -> pow(i, 2))
                .sum();
        return sqrt(squirtSum);
    }


    private int computeNewCoordinateForMultiply(Vector vector, int i) {
        int j = computeSecondIndex(i);
        return COORDINATE_ARR[i] * vector.COORDINATE_ARR[j]
                - COORDINATE_ARR[j] * vector.COORDINATE_ARR[i];
    }

    private int computeSecondIndex(int i) {
        int j = i + 1;
        if (j == COORDINATE_COUNT) {
            j = 0;
        }
        return j;
    }


    private int computePositionForCoordinateArr(int actualPosition) {
        int newPosition = (actualPosition > 0 ? actualPosition : COORDINATE_COUNT);
        return newPosition - 1;
    }

    private void checkInputArray() {
        if (COORDINATE_ARR != null && COORDINATE_ARR.length != COORDINATE_COUNT) {
            throw new IllegalArgumentException(ERROR_CREATION_MESSAGE);
        }
    }

    @Override
    public String toString() {
        return "Vector{" +
                "COORDINATE_ARR=" + Arrays.toString(COORDINATE_ARR) +
                ", LENGTH=" + LENGTH +
                '}';
    }
}
