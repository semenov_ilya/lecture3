package semenov.task1.figureAction;

import semenov.task1.figures.Figure;
import semenov.task1.figures.intefaces.Volumetric;

public class FigurePrinter implements FigureAction{

    @Override
    public void execute(Figure figure) {
        printFigureSquere(figure);
        printFigureVolume(figure);
    }


    private static void printFigureSquere(Figure figure) {
        System.out.println(figure.getSquere());
    }

    private static void printFigureVolume(Figure figure) {
        if (figure instanceof Volumetric) {
            Volumetric volumetricFigure = (Volumetric) figure;
            double volume = volumetricFigure.getVolume();
            System.out.println(volume);
        }
    }

}
