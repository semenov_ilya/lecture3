package semenov.task1.figureAction;

import semenov.task1.figures.Figure;

public interface FigureAction {
    void execute(Figure figure);
}
