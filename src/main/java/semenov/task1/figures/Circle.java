package semenov.task1.figures;

public class Circle extends Figure {
    private int raduis;


    public Circle(int raduis) {
        super();
        this.raduis = raduis;
    }

    public Circle(String name, int squere) {
        super(name);
        this.raduis = raduis;
    }


    public double getSquere() {
        double result = raduis * Math.PI;
        result = Math.pow(result, 2);
        return result;
    }
}
