package semenov.task1.figures;

import semenov.task1.figures.intefaces.Volumetric;

public class Parallelepiped extends Squere implements Volumetric {
    private double volume;


    public Parallelepiped(int sideLen) {
        super(sideLen);
    }

    Parallelepiped(String name, int sideLen) {
        super(name, sideLen);
    }


    @Override
    public double getSquere() {
        return 6 * super.getSquere();
    }

    public double getVolume() {
        return Math.pow(sideLen, 3);
    }
}
