package semenov.task1.figures;

public class Squere extends Figure {
    protected int sideLen;


    public Squere(int sideLen) {
        super();
        this.sideLen = sideLen;
    }

    Squere(String name, int sideLen) {
        super(name);
        this.sideLen = sideLen;
    }


    @Override
    public double getSquere() {
        return sideLen * sideLen;
    }
}
