package semenov.task1.figures;

public class Figure {
    protected String name;
    protected double squere;


    public Figure() {
    }

    public Figure(String name) {
        this.name = name;
    }

    public Figure(int squere) {
        this.squere = squere;
    }

    public Figure(String name, int squere) {
        this.name = name;
        this.squere = squere;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSquere() {
        return squere;
    }


}
