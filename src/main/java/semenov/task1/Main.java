package semenov.task1;

import semenov.task1.figureAction.FigureAction;
import semenov.task1.figureAction.FigurePrinter;
import semenov.task1.figures.Circle;
import semenov.task1.figures.Figure;
import semenov.task1.figures.Parallelepiped;
import semenov.task1.figures.Squere;

import java.util.ArrayList;
import java.util.List;

public class Main {
    final static int figureInt = 5;
    final static FigureAction figureAction = new FigurePrinter();

    public static void main(String[] args) {
        List<Figure> figureList = createFigureList();
        figureList.forEach(figure -> figureAction.execute(figure));
    }

    private static List<Figure> createFigureList() {
        List<Figure> figureList = new ArrayList<>();

        figureList.add(new Figure(figureInt));
        figureList.add(new Circle(figureInt));
        figureList.add(new Squere(figureInt));
        figureList.add(new Parallelepiped(figureInt));

        return figureList;
    }
}
